import styled from '@emotion/styled'

const Container = styled.div`
	${props => props.rootContainer && `
		max-width: 76rem;
		padding: 0 ${props.theme.metrics.horizontalSpace};
		margin-left: auto;
		margin-right: auto;
		width: 100%;

		@media print {
			max-width: 100%;
			padding: 0;
		}
	`}

	${props => props.phoneSmaller && `
		@media only screen and (max-width: 700px) {
			padding: 0 1rem;
		}
	`}

	${props => props.lessWide && `
		max-width: 70rem;
	`}

	${props => props.textWide && `
		max-width: 50rem;
	`}

	${props => props.sixtyWide && `
		max-width: 60rem;
	`}

	${props => props.wide && `
		max-width: 90rem;
	`}
`

export default Container
