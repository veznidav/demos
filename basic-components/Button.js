import React from 'react'
import styled from '@emotion/styled'
import typography from '../utils/typography'
import SmartLink from './SmartLink'

const Button = styled(({...props}) => (
	<SmartLink {...props} />
))`
	cursor: pointer;
	color: white;
	background-color: ${props => props.theme.colors.main};
	text-decoration: none;
	font-size: 0.555rem;
	border-radius: 0.888rem;
	padding: .4rem 2rem;
	display: inline-flex;
	align-items: center;
	margin: 1rem 0;
	transition: 200ms ease;
	text-transform: uppercase;
	letter-spacing: ${props => props.theme.metrics.spacedLetters};
	border: 1px solid ${props => props.theme.colors.main};
	font-weight: normal;
	-webkit-appearance: none;
	text-align: center;
	min-width: 7rem;
	justify-content: center;
	&:hover {
		background: ${props => props.theme.colors.mainDarker};
		border-color: ${props => props.theme.colors.mainDarker};
	}

	${props =>
		props.darker ?
			`
		background: #2B564F;
		border-color: #2B564F;
		&:hover {
			background: transparent;
			border-color: ${props.theme.colors.mainDarker};
			color: ${props.theme.colors.mainDarker}
	}
	}
	` : ''}

	${props =>
		props.bigger ?
			`
		font-size: 0.7rem;
		// height: ${typography.rhythm(1.5)};
		padding: .75rem 2rem;
		border-radius: 1.4rem;
	` : ''}

	${props =>
		props.outline ?
			`
		color: ${props.theme.colors.mainDarker};
		background-color: transparent;

		&:hover {
			color: white;
			background: ${props.theme.colors.mainDarker};
			border-color: ${props.theme.colors.mainDarker};
		}
	` : ''}

	${props =>
		props.whiteHover ?
			`
		transition: 200ms ease;
		&:hover {
			background: transparent;
			color: white;
			border-color: ${props.theme.colors.main};
		}
	` : ''}
`

const Btn = styled.button``

const ButtonNoLink = styled(
	({bigger, darker, outline, whiteHover, ...props}) => <Btn {...props} />
)`
	cursor: pointer;
	color: white;
	background-color: ${props => props.theme.colors.main};
	text-decoration: none;
	font-weight: 600;
	font-size: 0.555rem;
	border-radius: 0.888rem;
	padding: .4rem 2rem;
	display: inline-flex;
	align-items: center;
	margin: 1rem 0;
	transition: 200ms ease;
	text-transform: uppercase;
	letter-spacing: ${props => props.theme.metrics.spacedLetters};
	border: 1px solid ${props => props.theme.colors.main};
	font-weight: normal;
	-webkit-appearance: none;
	text-align: center;
	min-width: 8.9rem;
	justify-content: center;
	&:hover {
		background: ${props => props.theme.colors.mainDarker};
		border-color: ${props => props.theme.colors.mainDarker};
	}

	${props =>
		props.darker ?
			`
		background: #2B564F;
		border-color: #2B564F;
		&:hover {
			background: transparent;
			border-color: ${props.theme.colors.mainDarker};
			color: ${props.theme.colors.mainDarker}
	}
	}
	` : ''}

	${props =>
		props.bigger ?
			`
		font-size: 0.7rem;
		padding: .75rem 2rem;
		border-radius: 1.4rem;
	` : ''}

	${props =>
		props.outline ?
			`
		color: ${props.theme.colors.mainDarker};
		background-color: transparent;

		&:hover {
			color: white;
			background: ${props.theme.colors.mainDarker};
			border-color: ${props.theme.colors.mainDarker};
		}
	` : ''}

	${props =>
		props.whiteHover ?
			`
		transition: 200ms ease;
		&:hover {
			background: transparent;
			color: white;
			border-color: ${props.theme.colors.main};
		}
	` : ''}
`

export function QueriedButtonNoLink({label, bigger, ...rest}) {
	return (
		<ButtonNoLink bigger={bigger ? 1 : 0} {...rest}>
			{label}
		</ButtonNoLink>
	)
}

export function QueriedButton({data: {link, label}, bigger, ...rest}) {
	return (
		<Button to={link} bigger={bigger ? 1 : 0} {...rest}>
			{label}
		</Button>
	)
}

export default Button
