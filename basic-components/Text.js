import React, {createContext, useContext} from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled'

const TextStyled = styled.div`
	${props =>
		props.type === 'h1' &&
		`
		font-size: 3.333rem;
		font-weight: ${props.theme.fontWeights.bold};
		line-height: 1.2666;
	`}

	${props =>
		props.type === 'h1-reduced' &&
		`
		font-size: 2.8rem;
		font-weight: ${props.theme.fontWeights.bold};
		line-height: 1.2;
	`}

	${props =>
		props.type === 'h2' &&
		`
		font-size: 1.666rem;
		font-weight: ${props.theme.fontWeights.bold};
		line-height: 1.3;
	`}

	${props =>
		props.type === 'h3' &&
		`
		font-size: 1.111rem;
		font-weight: ${props.theme.fontWeights.bold};
		line-height: 1.3;
	`}

	${props =>
		props.type === 'h4' &&
		`
		font-size: 0.888rem;
	`}

	${props =>
		props.type === 'subheadline' &&
		`
		font-size: 1rem;
		opacity: 0.7;
	`}

	${props =>
		props.type === 'bodytext' &&
		`
		font-size: 0.777rem;
	`}

	${props =>
		props.type === 'aboveheadline' &&
		`
		font-size: 1.444rem;
	`}

	${props => `
		${props.breakpoint} {
			${
	props.type === 'h1'
		? `
				font-size: 1.666rem;
			`
		: ''
}

			${
	props.type === 'h1-reduced'
		? `
			font-size: 1.666rem;
		`
		: ''
}

				${
	props.type === 'h2'
		? `
				font-size: 1.222rem;
			`
		: ''
}

			${
	props.type === 'h3'
		? `
				font-size: 1.055rem;
			`
		: ''
}

			${
	props.type === 'h4'
		? `
				font-size: 0.777rem;
			`
		: ''
}

			${
	props.type === 'subheadline'
		? `
				font-size: 0.888rem;
			`
		: ''
}

			${
	props.type === 'bodytext'
		? `
					font-size: 0.777rem;
			`
		: ''
}

			${
	props.type === 'aboveheadline'
		? `
				font-size: 1rem;
			`
		: ''
}
		}
	`}
`

const breakpointContext = createContext(
	'@media only screen and (max-width: 1000px)'
)

const Text = React.forwardRef(
	(
		{
			className,
			breakpoint: propBreakpoint,
			asComponent,
			type,
			children,
			...rest
		},
		ref
	) => {
		const contextBreakpoint = useContext(breakpointContext)
		// If breakpoint is not specified as a prop use the one provided in context
		const breakpoint = propBreakpoint || contextBreakpoint

		const ToRender = TextStyled.withComponent(asComponent || 'div')

		return (
			<ToRender
				ref={ref}
				{...rest}
				breakpoint={breakpoint}
				className={className}
				type={type}
			>
				{children}
			</ToRender>
		)
	}
)

Text.propTypes = {
	type: PropTypes.oneOf([
		'h1',
		'h1-reduced',
		'h2',
		'h3',
		'h4',
		'subheadline',
		'bodytext',
		'aboveheadline',
	]),
	className: PropTypes.string,
	breakpoint: PropTypes.string,
	children: PropTypes.node.isRequired,
	asComponent: PropTypes.string,
}

Text.defaultProps = {
	type: 'bodytext',
	breakpoint: null,
	asComponent: 'div',
}

export default Text
export {breakpointContext}
