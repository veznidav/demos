import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import {
  Section, SectionBody, Button, CSSGrid, Table, Loader,
} from '@nef/core';
import { isEmpty } from 'lodash';
import { apiClientQE } from '../../api/apiClient';
import ToastTemplate from '../ToastTemplate';
import DetailsCustomCell from './DetailsCustomCell';
import log from '../../commons/services/Logger';
import { stringifyObject } from '../Utils';

export default function NedBboDetails({ sequence, handleClose }) {
  const { t } = useTranslation();
  const tableSettings = useSelector((state) => state.tableSettings);
  const userInfo = useSelector((state) => state.userInfo);
  const tableData = useSelector((state) => state.tableData);
  const [data, setData] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const { filterFormData } = tableData;
  const {
    exchange, searchCriteria, protocols,
  } = filterFormData;

  useEffect(() => {
    const getNedBboDetails = async () => {
      setIsLoading(true);
      const bboColumns = tableSettings.columns[exchange].bbo.filter((col) => !col.hiddenFromDetails && !col.hiddenFromUI); // exclude columns that are set to be hidden in Details or in the UI
      
      /* lbbo columns use Header, so the structure is: 
       * [{ Header: 'BID', columns: [...] }, { Header: 'ASK', columns: [...] }]
       * the next line gets all 'columns' arrays and generates a single array from their values
       * for instance from: [[bidBadge, bidSize], [askBadge, askSize]] becomes: [bidBadge, bidSize, askBadge, askSize] */
      const lbboColumns = tableSettings.columns[exchange].lbbo.map(item => item.columns).flat();
      
      const request = {
        date: searchCriteria.date,
        protocols,
        exchange,
        bboColumns,
        abboColumns: [], // no abbo columns for NED
        lbboColumns,
        searchCriteria: {
          sequence,
          event: "bbo",
        },
        openIdRefreshToken: userInfo.refreshToken,
      };

      try {
        const bboDetails = await apiClientQE.post(process.env.REACT_APP_BBO_DETAILS, request);

        setData(bboDetails.data);
        setIsLoading(false);
      } catch (e) {
        setIsLoading(false);
        toast.error(
          <ToastTemplate type="error" subheader={t('somethingWentWrong')}>
            {t('fetchDetailsFailed')}
          </ToastTemplate>,
          { autoClose: false }
        );
        log.error(`Get NED BBO details request failed. URL: ${process.env.REACT_APP_BBO_DETAILS}. Request: ${stringifyObject(request)}. Error message: ${e?.message}`);
      }
    };
    getNedBboDetails();
  }, [sequence]);

  // on componnt unmount
  useEffect(() => {
    return () => handleClose();
  }, []);

  const getLbboTableHeaders = () => {
    if (tableSettings?.columns[exchange]?.lbbo) {
        return tableSettings.columns[exchange].lbbo.map((header) => ({
            ...header,
            columns: header.columns.map((col) => ({ 
              Header: () => <span data-testid={`lbbo-column-name-${col.name}`}>{col.friendlyName}</span>,
              accessor: col.name, 
              Cell: (props) => <DetailsCustomCell {...props} />,
            }))
        }));
    }
    return [];
  }

  const getTables = () => {
    let tableHeaders = [];
    if (data?.bbo?.columns && data.bbo.columns.length > 0) {
      tableHeaders = data.bbo.columns.map((col) => (
        {
          Header: () => <span data-testid={`detail-column-name-${col.name}`}>{col.friendlyName}</span>,
          accessor: col.name,
          Cell: ({ value }) => {
            return <span className="custom-cell" data-testid={`detail-cell-${col.name}`}>{value}</span>;
          },
        }
      ));
    }
    // Split the table headers into n tables, where each table has at most 10 columns.
    // This prevents the tables from requiring excessive horizontal scrolling.
    const tables = [];
    while (tableHeaders.length) {
      tables.push(<Table
        className="user-list-table-block"
        data={data.bbo.rows}
        columns={tableHeaders.splice(0, 10)} // 10 cols per table
        showPagination={false}
        minRows={0}
      />);
    }
    return tables;
  };

  if (isLoading) {
    return (
      <CSSGrid.Container colSpan={12}>
        <div className="loader-area">
          <Loader isLoading={true} />
        </div>
      </CSSGrid.Container>
    );
  }

  return (
    <CSSGrid.Container colSpan={12} data-testid="bbo-details">
      {data && !isEmpty(data.bbo) ? (
        <Section className="section">
          <div className="table-section-header">
            <div className="table-header-info detail-header" data-testid="bbo-details-header">
              {t('details')}
              :
            </div>
            <div className="table-header-export">
              <Button
                size="sm"
                outline={true}
                color="primary"
                onClick={() => handleClose()}
                data-testid="bbo-details-button-close"
              >
                {t('close')}
              </Button>
            </div>
          </div>
          <SectionBody className="table-section-body">
            {getTables()}
            {!isEmpty(data.lbbo.rows)
                && (
                  <>
                    <h5 className="detail-header">
                      {t('lbbo')}
                      {' '}
                      {t('details')}
                      :
                    </h5>
                    <Table
                      className="user-list-table-block"
                      data={data.lbbo.rows}
                      defaultPageSize={data.lbbo.rows?.length}
                      columns={getLbboTableHeaders()}
                      showPagination={false}
                      minRows={1}
                    />
                  </>
                )}
          </SectionBody>
        </Section>
      ) : (
        <Section>
          <div className="table-section-header">
            <div className="table-header-info detail-header" data-testid="bbo-details-na">{t('detailsNA')}</div>
            <div className="table-header-export">
              <Button
                size="sm"
                outline={true}
                color="primary"
                onClick={() => handleClose()}
                data-testid="bbo-details-button-close"
              >
                {t('close')}
              </Button>
            </div>
          </div>
        </Section>
      )}
    </CSSGrid.Container>
  );
}
NedBboDetails.propTypes = {
  sequence: PropTypes.number.isRequired,
  handleClose: PropTypes.func.isRequired,
};
