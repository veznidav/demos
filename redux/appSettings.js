import { createSlice } from '@reduxjs/toolkit';

export const initialState = {
  toggleUIEnabled: false, // If enabled, the user menu will enable toggling between UI modes (e.g. Regular scroll vs. Infinite scroll)
  environmentAlertVisible: true,
};

const appSettings = createSlice({
  name: 'appSettings',
  initialState,
  reducers: {
    setToggleUIEnabled: (state, action) => {
      state.toggleUIEnabled = action.payload;
    },
    setEnvironmentAlertVisible: (state, action) => {
      state.environmentAlertVisible = action.payload;
    }
  },
});

export const {
  setToggleUIEnabled,
  setEnvironmentAlertVisible,
} = appSettings.actions;

export default appSettings.reducer;
