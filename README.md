# Demos

## Files

- basic-components
    - Basic, reusable components. They were created because the client did not want to depend on a 3rd party component library. Created using Styled components.
- components
    - a component that is composed of other components 
    - api call + error handling
- custom-hooks
    - I like creating custom hooks when it makes sense. An elegant way to share the same logic in multiple components.
- redux
    - When I use Redux, I use the Redux toolkit way. Simplifies the redux flow a lot.
- tests
    - very basic set of tests    

