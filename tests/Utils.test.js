import { formatDateTimeRange, stringifyObject } from '../components/Utils';
import moment from 'moment';

describe('formatDateTimeRange', () => {
    it('valid input date and time', () => {
        const input = moment('2021-05-20 09:00:00');
        const expectedOutput = '2021-05-20T09:00:00.000';
        expect(formatDateTimeRange(input)).toEqual(expectedOutput);
    });
    it('valid input date, no time', () => {
        const input = moment('2021-05-30');
        const expectedOutput = '2021-05-30T00:00:00.000';

        expect(formatDateTimeRange(input)).toEqual(expectedOutput);
    });
    it('invalid input', () => {
        const input = '';
        const expectedOutput = null;

        expect(formatDateTimeRange(input)).toEqual(expectedOutput);
    });
})

describe('stringifyObject', () => {
    it('Valid values are OK', () => {
        const input = { ID: 34, message: "test msg" };
        const expectedOutput = JSON.stringify(input);
        expect(stringifyObject(input)).toEqual(expectedOutput);
    });
    it('Authorization is hidden', () => {
        const input = { Authorization: "adfkljFi434F" };
        const expectedOutput = JSON.stringify({ Authorization: "hidden" });
        expect(stringifyObject(input)).toEqual(expectedOutput);
    });
    it('openIdRefreshToken is hidden', () => {
        const input = { openIdRefreshToken: "2389482984" };
        const expectedOutput = JSON.stringify({ openIdRefreshToken: "hidden" });
        expect(stringifyObject(input)).toEqual(expectedOutput);
    });
    it('Mixed is OK', () => {
        const input = { openIdRefreshToken: "2389482984", Authorization: "adfkljFi434F", ID: 34, message: "test msg" };
        const expectedOutput = JSON.stringify({ openIdRefreshToken: "hidden", Authorization: "hidden", ID: 34, message: "test msg" });
        expect(stringifyObject(input)).toEqual(expectedOutput);
    });
})
