import { useSelector } from "react-redux";
import { getProfile } from "../redux/selectors/firebase";
import { USER_TYPES } from "../constants";

export const useProfile = () => {
  const profile = useSelector(getProfile);
  const profileImageUrl = profile.userType === USER_TYPES.STARTUP ? profile.logo : profile.avatarUrl;
  return {
    profileImageUrl,
    userType: profile.userType,
    isCandidate: profile.userType === USER_TYPES.CANDIDATE,
    savedJobs: profile.savedJobs || [],
  };
};
